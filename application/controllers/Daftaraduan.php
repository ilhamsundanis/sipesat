<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftaraduan extends CI_Controller {
	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('seputar_aduan');
		$this->load->view('common/footer');
	}
}
