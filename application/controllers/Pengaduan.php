<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan extends CI_Controller {
	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('pengaduan');
		$this->load->view('common/footer');
	}
}
