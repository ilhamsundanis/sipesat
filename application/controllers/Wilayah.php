<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends CI_Controller {
	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('sipesat_wilayah');
		$this->load->view('common/footer');
	}
}
