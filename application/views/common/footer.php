<!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 footer-links">
            <img src="<?php echo base_url();?>assets/img/logo_sipesat.png" style="width: 90%;">
          </div>
          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>SIPESAT</h3>
            <p>
              Jl.Aman No. 4, Kel Tengah <br />
              Kec. Cibinong,
              Bogor 16914 <br><br>
              <strong>Telp:</strong> (021) 87908636<br>
              <strong>Email:</strong> satpolpp@bogorkab.go.id<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Navigasi</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Beranda</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Pengaduan</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Berita / Informasi</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Sipesat Wilayah</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Seputar Aduan</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Cek Pengaduan</h4>
            <p>Masukan nomor tiket pengaduan Anda untuk dapat melihat status penanganan</p>
            <form action="" method="post">
              <input type="text" name="no_tiket" class="form-control" placeholder="SPS-PM000001"><input type="submit" value="Cek">
            </form>
          </div>


        </div>
      </div>
    </div>

    <div class="container d-lg-flex py-4">

      <div class="me-lg-auto text-center text-lg-start">
        <div class="copyright">
          &copy; Copyright <strong><span>SATPOLPP KAB BOGOR</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
          
          Designed by <a href="http://indoaksesmedia.co.id/">Indo Akses Media</a>
        </div>
      </div>
      <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-youtube"></i></a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>