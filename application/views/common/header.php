<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SIPESAT - Sistem Pengaduan Satuan Polisi Pamong Praja | Kabupaten Bogor</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url();?>assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center">
    <div class="container d-flex justify-content-center justify-content-md-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope d-flex align-items-center"><a href="mailto:contact@example.com">satpolpp@bogorkab.go.id</a></i>
        <i class="bi bi-phone d-flex align-items-center ms-4"><span>(021) 87908636</span></i>
        <i class='bi bi-bank d-flex align-items-center ms-4'><span>Jl.Aman No. 4, Kel Tengah, Cibinong, Bogor</span></i>
      </div>

      <div class="cta d-none d-md-flex align-items-center">
        <a target="_blank" href="https://satpolpp.bogorkab.go.id/" class="scrollto"><i class="bi bi-chevron-double-right"></i> Portal Utama</a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <!-- <h1><a href="index.html">Flexor</a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo -->
         <a href="<?php echo base_url();?>"><img src="assets/img/logo_sipesat.png" alt="" class="img-fluid"></a>
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto" style="font-family:'Tahoma';" href="<?php echo base_url();?>">BERANDA</a></li>
          <li><a class="nav-link scrollto" href="<?php echo base_url('pengaduan');?>">PENGADUAN</a></li>
          <li><a class="nav-link scrollto" href="<?php echo base_url('berita');?>">BERITA / INFORMASI</a></li>
          <li><a class="nav-link scrollto " href="<?php echo base_url('wilayah');?>">SIPESAT WILAYAH</a></li>
          <li><a class="nav-link scrollto" href="<?php echo base_url('daftaraduan');?>">SEPUTAR ADUAN</a></li>
          
          <!-- <li><a class="nav-link scrollto" href="<?php echo base_url('kontak');?>">HUBUNGI</a></li> -->
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->