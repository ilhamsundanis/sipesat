<main id="main">

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container">

        <div class="row">
          <div class="col-xl-4 col-lg-5" data-aos="fade-up">
            <div class="content">
              <h3>Sampaikan laporan Anda langsung kepada kami yang berwenang</h3>
              <p>
                Layanan Aspirasi dan Pengaduan Online Masyarakat Kabupaten Bogor 
              </p>
              <!-- <div class="text-center">
                <a href="#" class="more-btn">ADUKAN SEKARANG <i class="bx bx-chevron-right"></i></a>
              </div> -->
            </div>
          </div>
          <div class="col-xl-8 col-lg-7 d-flex">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-pencil"></i>
                    <h4>Tulis Aduan</h4>
                    <p>Laporkan keluhan atau aspirasi anda dengan jelas dan lengkap</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-cube-alt"></i>
                    <h4>Proses Tindak Lanjut</h4>
                    <p>Dalam beberapa hari, instansi akan menindaklanjuti laporan Anda</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-images"></i>
                    <h4>Tuntaskan</h4>
                    <p>Laporan Anda akan terus ditindaklanjuti hingga terselesaikan</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Why Us Section -->

      <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">Sampaikan Aduan Anda</h2>
          <p data-aos="fade-up">Segera sampaikan langsung aduan kepada kami agar segera di tindak lanjut oleh kami</p>
        </div>

        

        <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="300">
          <div class="col-xl-9 col-lg-12 mt-4">
            <form action="" method="post" role="form" class="php-email-form">
              <div class="form-group mt-3">
                <input type="text" class="form-control" name="nama_pelapor" id="nama_pelapor" placeholder="Nama Lengkap Anda*" required>
              </div>
              <br />
              <div class="row">
                <div class="col-md-6 form-group">
                  <input type="text" name="no_telp" class="form-control" id="no_telp" placeholder="Nomor Telp" required autocomplete="off">
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email anda" required autocomplete="off">
                </div>
              </div>
              <div class="form-group mt-3">
                <textarea class="form-control" name="isi_aduan" rows="5" placeholder="Tuliskan Aduan Anda*" required></textarea>
              </div>
              <br />
              <div class="row">
                <div class="col-md-6 form-group">
                 <select name="kec_id" class="form-control">
                   <option selected=""> -- Pilih Kecamatan -- </option>
                 </select>
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <select name="desa_id" class="form-control">
                   <option selected=""> -- Pilih Desa -- </option>
                 </select>
                </div>                
              </div>
              <div class="form-group mt-3">
                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat lengkap*" required>
              </div>
              <br />
              <div class="row">
                <div class="col-md-3 form-group">
                  <label>Upload Dokumentasi</label>
                  <input type="file" name="no_telp" class="form-control" id="no_telp" placeholder="Nomor Telp" required autocomplete="off">
                </div>
                <div class="col-md-3 form-group mt-3 mt-md-0">
                  <label>Klasifikasi</label>
                   <select name="desa_id" class="form-control">
                   <option selected=""> -- Klasifikasi Aduan --</option>
                   <option value="sangatpenting">Sangat Penting</option>
                   <option value="penting">Penting</option>
                 </select>
                </div>
                <div class="col-md-6 form-group">
                  <label>NIK</label>
                  <input type="text" name="nik" class="form-control" id="nik" placeholder="Input NIK Pelapor" required autocomplete="off">
              </div>            
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <br />
              <div class="text-center"><button type="submit">KIRIM ADUAN!</button></div>
            </form>
          </div>
        </div>

        <div class="row justify-content-center">

          <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up">
            <div class="info-box">
              <i class="bx bx-map"></i>
              <h3>Kantor Layanan</h3>
              <p>Jl.Aman No. 4, Kel Tengah, Cibinong, Bogor</p>
            </div>
          </div>

          <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up" data-aos-delay="100">
            <div class="info-box">
              <i class="bx bx-envelope"></i>
              <h3>Email</h3>
              <p>satpolpp@bogorkab.go.id<br>sipesatbogor@gmail.com</p>
            </div>
          </div>
          <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up" data-aos-delay="200">
            <div class="info-box">
              <i class="bx bx-phone-call"></i>
              <h3>Telp / WA</h3>
              <p>(021) 87908636<br>+62 8123456789</p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Contact Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <div class="container">

        <div class="row">
          <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch position-relative" data-aos="fade-right">
            <a href="https://www.youtube.com/watch?v=dAgVe-6rEXU&t=70s" class="glightbox play-btn mb-4"></a>
          </div>

          <div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
            <h3 data-aos="fade-up">Sistem Pengaduan Satuan Polisi Pamong Praja</h3>
            <p data-aos="fade-up">Merupakan layanan pengaduan bagi masyarakat untuk melaporkan adanya Pelanggaran PERDA / PERKADA di Kabupaten Bogor, Sistem ini terbuka untuk siapapun</p>

            <div class="icon-box" data-aos="fade-up">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">Klasifikasi Pengaduan</a></h4>
              <p class="description">Sebelum anda melakukan pengaduan, fahami kategori klasifikasi pengaduannya terlebih dahulu dan pastikan sesuai.</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">Nomor Tiket Pengaduan</a></h4>
              <p class="description">Anda akan mendaptkan nomor tiket pengaduan setelah anda mengirimkan aduan sesuai dengan form yang tertera. Simpan nomor tiket untuk melakukan pengecekan proses penanganan</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
              <div class="icon"><i class="bx bx-atom"></i></div>
              <h4 class="title"><a href="">Layanan Cepat</a></h4>
              <p class="description">Pastikan Nomor telp yang anda masukan aktif dan benar, untuk kontak yang bisa dihubungi oleh Tim Gerak Cepat Polisi Pamong Praja</p>
            </div>

          </div>
        </div>

      </div>
    </section><!-- End About Section -->

     <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
      <div class="container">

        <div class="row">

          <div class="col-lg-3 col-md-6">
            <div class="count-box">
              <i class="bx bx-user-md"></i>
              <span data-purecounter-start="0" data-purecounter-end="85" data-purecounter-duration="1" class="purecounter">85</span>
              <p>Laporan Aduan</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
            <div class="count-box">
              <i class="bx bx-hospital"></i>
              <span data-purecounter-start="0" data-purecounter-end="18" data-purecounter-duration="1" class="purecounter">120</span>
              <p>Belum Ditangani</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
            <div class="count-box">
              <i class="bi bi-flask"></i>
              <span data-purecounter-start="0" data-purecounter-end="12" data-purecounter-duration="1" class="purecounter">20</span>
              <p>Sudah Ditangani</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
            <div class="count-box">
              <i class="bx bx-award"></i>
              <span data-purecounter-start="0" data-purecounter-end="150" data-purecounter-duration="1" class="purecounter">30</span>
              <p>Tidak ditangani</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Counts Section -->


    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
      <div class="container" data-aos="fade-up">

        <div class="clients-slider swiper-container">
          <div class="swiper-wrapper align-items-center">
            <div class="swiper-slide"><img src="assets/img/pancakarsa/ADAB.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="assets/img/pancakarsa/BANGUN.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="assets/img/pancakarsa/CERDAS.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="assets/img/pancakarsa/MAJU.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="assets/img/pancakarsa/SEHAT.png" class="img-fluid" alt=""></div>
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Clients Section -->



    <!-- ======= Values Section ======= -->
    <section id="values" class="values">
      <div class="container">

        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card" style="background-image: url(assets/img/blog/blog-1.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="">Judul Berita 1</a></h5>
                 <p class="card-text">Ini merupakan contoh untuk narasi berita atau informasi apapun ya jadi apapun itu ya cuma contoh </p>
                <div class="read-more"><a href="#"><i class="bi bi-arrow-right"></i> Baca Selengkapnya</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="100">
            <div class="card" style="background-image: url(assets/img/blog/blog-2.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="">Judul Berita 2</a></h5>
                <p class="card-text">Ini merupakan contoh untuk narasi berita atau informasi apapun ya jadi apapun itu ya cuma contoh </p>
                <div class="read-more"><a href="#"><i class="bi bi-arrow-right"></i> Baca Selengkapnya</a></div>
              </div>
            </div>

          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="200">
            <div class="card" style="background-image: url(assets/img/blog/blog-3.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="">Judul Berita 3</a></h5>
                 <p class="card-text">Ini merupakan contoh untuk narasi berita atau informasi apapun ya jadi apapun itu ya cuma contoh </p>
                <div class="read-more"><a href="#"><i class="bi bi-arrow-right"></i> Baca Selengkapnya</a></div>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="300">
            <div class="card" style="background-image: url(assets/img/blog/blog-4.jpg);">
              <div class="card-body">
                <h5 class="card-title"><a href="">Judul Berita 4</a></h5>
                 <p class="card-text">Ini merupakan contoh untuk narasi berita atau informasi apapun ya jadi apapun itu ya cuma contoh </p>
                <div class="read-more"><a href="#"><i class="bi bi-arrow-right"></i> Baca Selengkapnya</a></div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Values Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container position-relative" data-aos="fade-up">

        <div class="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">

            <div class="swiper-slide">
              <div class="testimonial-item">
                <!-- <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt=""> -->
                <h3>Abdu Faisal</h3>
                <h4>Desa Kadumanggu, Babakan Madang</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Poé Saptu pasosoré kuring némbongkeun rapot ka bapa anu keur diuk nyalsé di tepas. Bapa nempoan rapot kuring meni tenget. Sabot bapa keur nengetan rapot, kuring ngabéjakeun yén kuring téh jadi juara kelas. Bapa katempo bungaheun pisan ngadéngé béja kitu téh.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <!-- <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt=""> -->
                <h3>Michael Ujang</h3>
                <h4>Desa Koleang, Rancabungur</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Keur kitu ti jero kadéngé indung nyelengkeung ngajakan liburan ka laut. Pokna téh itung-itung hadiah keur kuring anu geus jadi juara kelas. Bapa satujueun kabeneran isukan poé Ahad peré digawé. Atuh kuring kacida bungahna ngadéngé kitu téh.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <!-- <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt=""> -->
                <h3>Nyai Angel</h3>
                <h4>Desa Sipak, Jasinga</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                 Singget carita, isukna isuk-isuk kénéh kuring geus tatan-tatan. Indung geus uprek masak, pon kitu deui bapa geus ngahaneutan mobil. Sakira tabuh 7 kuring miang rék ngajugjud pantéy Santolo di Pameungpeuk anu pernahna di béh kidul Kabupaten Garut.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <!-- <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt=""> -->
                <h3>Siti Sopiah</h3>
                <h4>Cirimekar, Cibinong</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Barang nepi ka Gunung Gelap anu jauh kaditu kadieu, mobil bapa ngadadak mogok. Sihoréng mobil téh béngsinna béak. Bapa pohoeun teu kungsi ngeusian bengsin di POM. Sabot keur tingharuleng, torojol aya motor ngaliwat. Ku bapa dipegat, maksudna rék milu nepika hareup sugan aya nu dagang bengsin. Nu boga motor téh bageur, bapa dibawa. Kuring jeung indung geuwat asup ka jero mobil bari ngonci manéh ti jero kusabab keueung.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <!-- <img src="assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt=""> -->
                <h3>John F Keheula</h3>
                <h4>Puspanegara, Citeureup</h4>
                <p>
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Heuleut sapuluh menit bapa geus balik deui tumpak ojég, ngajingjing jarikén dieusi béngsin. Sanggeus dieusian béngsin mah mobil téh teu sakara-kara hidup deui. Kuring ngemplong bari ngucap sukur. Lalampahan ogé dituluykeun. Sajajalan kuring galécok sakur nu katempo sagala ditanyakeun da puguh kakara ngaliwat ka tebéh dinya.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->

  





    <!-- ======= F.A.Q Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">F.A.Q</h2>
          <p data-aos="fade-up">Ketentuan Penggunanaan <em>(Terms of Use)</em> Layanan SIPESAT.</p>
        </div>

        <div class="faq-list">
          <ul>
            <li data-aos="fade-up">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" class="collapse" data-bs-target="#faq-list-1">Ketentuan Penggunaan? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-1" class="collapse show" data-bs-parent=".faq-list">
                <p>
                  Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-2" class="collapsed">Hak hak Penggunaan? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-2" class="collapse" data-bs-parent=".faq-list">
                <p>
                  Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-3" class="collapsed">Syarat Penggunaan Aplikasi Sipesat? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-bs-parent=".faq-list">
                <p>
                  Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-4" class="collapsed">Bagaimana Tindak Lanjut pada SIPESAT? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-bs-parent=".faq-list">
                <p>
                  Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-5" class="collapsed">Kerahasiaan dan Informasi Pribadi? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-5" class="collapse" data-bs-parent=".faq-list">
                <p>
                  Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque.
                </p>
              </div>
            </li>

          </ul>
        </div>

      </div>
    </section><!-- End F.A.Q Section -->



</main><!-- End #main -->