
  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">
        <ol>
          <li><a href="<?php echo base_url();?>">Beranda</a></li>
          <li>Pengaduan</li>
        </ol>
        <h2>Layanan Pengaduan</h2>
      </div>
    </section><!-- End Breadcrumbs -->

    <section class="inner-page pt-3">
      <div class="container">
        
        <table class="table">
          <thead>
            <th>NO</th>
            <th>NAMA</th>
            <th>KECAMATAN</th>
            <th>DESA</th>
            <th>ADUAN</th>
          </thead>
          <tbody>
            <?php
                for ($i=1; $i <= 10 ; $i++) { 
            ?>
            <tr>
              <td><?php echo $i;?></td>
              <td>Nama Orang ke <?php echo $i;?></td>
              <td>Kecamatan Orang Ke <?php echo $i;?></td>
              <td>Desa Orang Ke <?php echo $i;?></td>
              <td>Contoh Isi Aduan Orang Ke <?php echo $i;?></td>
            </tr>
          <?php }?>
          </tbody>
        </table>
      </div>
    </section>

  </main><!-- End #main -->